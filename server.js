const express = require('express');
const bodyparser = require('body-parser');
const cors = require('cors');
const server = express();
const todolistRouter = require('./src/router/todolistRouter');

server.use(cors());
server.use(bodyparser.urlencoded({ extended: true }));
server.use(bodyparser.json());

server.use('/api', todolistRouter);
server.listen(3000, () => {
    console.log('API rodando na porta 3000');
});