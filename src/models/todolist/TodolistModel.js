const db = require('../../config/dbConnect');

module.exports = class Todolist{

    static getTasks(limit, offset, callback){
        return db.query('Select * From to_do_list Limit ? Offset ?', [limit, offset], callback);
    }

    static getTaskId(id, callback){
        return db.query('Select * From to_do_list Where id = ?', [id], callback);
    }

    static addTask(task, callback){
        return db.query('Insert Into to_do_list (descricao) Values (?)', [task.descricao], callback);
    }

    static delTask(id, callback){
        return db.query('Delete From to_do_list Where id = ?', [id], callback);
    }

    static putTask(task, callback){
        return db.query('Update to_do_list Set descricao = ? Where id = ?', [task.descricao, task.id], callback);
    }
};