module.exports = class Pagination{
    constructor(currentPage, perPage){
        this.perPage = perPage;
        this.currentPage = parseInt(currentPage);
        this.previousPage = this.currentPage - 1;
        this.offset  = this.currentPage > 1 ? this.previousPage * this.perPage : 0;
    }
}