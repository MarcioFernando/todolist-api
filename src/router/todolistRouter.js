const express = require('express');
const router = express.Router();
const TodolistModel = require('../models/todolist/TodolistModel');
const Response = require('../models/ResponseClass');
const Pagination = require('../models/todolist/PaginationClass');

//Lista tarefas com paginação
router.get('/tasks/:page?', (req, res) => {
    const page_id = parseInt(req.params.page);
    //Página atual
    const currentPage = page_id > 0 ? page_id : 1;
    //Ítens por página
    const itemPage = 10;
    const Paginate = new Pagination(currentPage, itemPage);
    TodolistModel.getTasks(Paginate.perPage, Paginate.offset, (erro, retorno) => {
        let resposta = new Response();
        if(erro){
            resposta.erro = true;
            resposta.msg = 'Ocorreu um erro!';
            console.log(`Erro: ${erro}`);
        }
        else{
            resposta.dados = retorno;
        }
        res.json(resposta);
    });
});

//Obter tafera por ID
router.get('/task/:id', (req, res) => {
    TodolistModel.getTaskId(req.params.id, (erro, retorno) => {
        let resposta = new Response();
        if(erro){
            resposta.erro = true;
            resposta.msg = 'Ocorreu um erro!';
            console.log(`Erro: ${erro}`);
        }
        else{
            resposta.dados = retorno;
        }
        res.json(resposta);
    });
});

//Adicionar tafera
router.post('/task', (req, res) => {
    TodolistModel.addTask(req.body, (erro, retorno) => {
        let resposta = new Response();
        if(erro){
            resposta.erro = true;
            resposta.msg = 'Ocorreu um erro!';
            console.log(`Erro: ${erro}`);
        }
        else{
            if(retorno.affectedRows > 0){
                resposta.msg = 'Tarefa adicionada com sucesso!';
            }
            else{
                resposta.erro = true;
                resposta.msg = 'Operação não realizada!';
            }
        }
        res.json(resposta);
    });
});

//Editar tarefa
router.put('/task', (req, res) => {
    TodolistModel.putTask(req.body, (erro, retorno) => {
        let resposta = new Response();
        if(erro){
            resposta.erro = true;
            resposta.msg = 'Ocorreu um erro!';
            console.log(`Erro: ${erro}`);
        }
        else{
            if(retorno.affectedRows > 0){
                resposta.msg = 'Tarefa editada com sucesso!';
            }
            else{
                resposta.erro = true;
                resposta.msg = 'Operação não realizada!';
            }
        }
        res.json(resposta);
    });
});

//Deletar tarefa
router.delete('/task/:id', (req, res) => {
    TodolistModel.delTask(req.params.id, (erro, retorno) => {
        let resposta = new Response();
        if(erro){
            resposta.erro = true;
            resposta.msg = 'Ocorreu um erro!';
            console.log(`Erro: ${erro}`);
        }
        else{
            if(retorno.affectedRows > 0){
                resposta.msg = 'Tarefa excluída com sucesso!';
            }
            else{
                resposta.erro = true;
                resposta.msg = 'Operação não realizada!';
            }
        }
        res.json(resposta);
    });
});

module.exports = router;