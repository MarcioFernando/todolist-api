# API To Do List

## Instalação

1. Faça o clone deste projeto com `git clone https://MarcioFernando@bitbucket.org/MarcioFernando/todolist-api.git`
2. Entre na pasta do projeto e instale as dependências com `npm install`


## Criando o banco de dados

O banco de dados para a aplicação é o MySql, para criar o banco é necessário executar o seguinte código abaixo.

```sql

    CREATE DATABASE todolist;
  
    CREATE TABLE `to_do_list` (
      `id` int(11) NOT NULL AUTO_INCREMENT,
      `descricao` varchar(100) NOT NULL,
      `feito` TINYINT NOT NULL DEFAULT 0,
      `data` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
      PRIMARY KEY (`id`)
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8;

```

## Iniciando a API

Antes de iniciar a API verifique os dados para a conexão que está no arquivo _dbConnect.js_ no caminho _"src/database"_.

Na pasta raiz do projeto digite o seguinte comando para iniciar o servidor `npm start`, uma mensgem será exibida _"API rodando na porta 3000"_.


## Testando a API

A API roda sobre o seguinte endereço `http://localhost:3000/api`, para testar utilizaremos o [Postman](https://www.getpostman.com/), uma ferramenta que permite realizar requisições via HTTP.


### Cadastrar uma nova tarefa

Para cadastrar uma nova tarefa através do Postman basta selecionar a opção _POST_ no endereco `http://localhost:3000/api/task` e logo baixo escolher a opção _Body_ e na sequência _x-www-form-urlencoded_ preenchendo o campo _KEY_ com _"descricao"_ (Campo da tabela) e _VALUE_ com algum ítem de tarefa e em seguida clique em _Send_. Veja o exemplo a baixo.
![link](src/public/imgs/post.jpg)


### Listar tarefas com paginação

Para listar as tarefas usando o Postman basta selecionar a opção _GET_ no endereço `http://localhost:3000/api/tasks`, em seguida clique em _Send_. Para alterar as páginas basta adicionar um número ao final da requisição. Veja o exemplo abaixo. 
![link](src/public/imgs/getpg.jpg)


### Listar tarefa por ID

Para listar tarefa utilizando uma busca por ID basta selecionar a opção _GET_ no endereço `http://localhost:3000/api/task/10`, em seguida clique em _Send_. No exemplo o ID para buscar é _10_. Veja o exemplo abaixo. 
![link](src/public/imgs/getid.jpg)


### Editar uma tarefa

Para editar uma tarefa basta selecionar a opção _PUT_ no seguinte endereço `http://localhost:3000/api/task` e logo baixo escolher a opção _Body_ e na sequência _x-www-form-urlencoded_ preenchendo o campo _KEY_ com _"descricao"_ e _VALUE_ com um novo valor, o mesmo para _id_ com valor da tarefa que deseja editar, em seguida clique em _Send_. Veja o exemplo a baixo.
![link](src/public/imgs/edit.jpg)


### Remover uma tarefa

Para remover uma ferramaneta basta selecionar a opção _DELETE_ no seguinte endereço `http://localhost:3000/api/task/25`, em seguida clique em _Send_. No exemplo _25_ é o ID da tarefa a ser excluído. Veja o exemplo abaixo.
![link](src/public/imgs/delete.jpg)